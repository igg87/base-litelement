import { LitElement, html } from 'lit-element'

class FichaPersona extends LitElement {
    
    static get properties () {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String},
            photo: {type: Object}
        }
    }

    // LitElement Lifecycle
    constructor () {
        // Funcion del ciclo de vida de LitElements -> Se llama antes cargar el componente
        // Inicializacion de variables
        // super llama al mismo metodo (constructor) en la clase padre (LitElement)
        super()

        this.name = "Prueba nombre"
        this.yearsInCompany = 12
        this.updatePersonInfo()
    }

    render () {
        // El render pinta lo que se visualiza por pantalla
        // La comilla ` permite presentar el html en modo multi-linea
        return html`
            <div>
                <label>Nombre completo</label>
                <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br />
                <label>Años en la empresa</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
                <br />
                <input type="text" value="${this.personInfo}" disabled></input>
                <br />
            </div>
        `
    }

    updated (changedProperties) {
        // Funcion del ciclo de vida de LitElements -> Se llama cuando se ha actualizado y pintado el componente
        // Recorremos todas las propiedades
        /*
        changedProperties.forEach((oldValue, propName) => {
            console.log("Propiedad " + propName + " cambia valor, anterior era " + oldValue)
        })
        */

        // Buscamos cambios en una variable concreta
        if (changedProperties.has("name")) {
            console.log("Propiedad name cambia de " + changedProperties.get("name") + " a " + this.name)
        }
        if (changedProperties.has("yearsInCompany")) {
            console.log("Propiedad yearsInCompany cambia de " + changedProperties.get("yearsInCompany") + " a " + this.yearsInCompany)
            this.updatePersonInfo()
        }
    }

    // Metodos propios
    updateName (e) {
        console.log("updateName")
        // Cambiamos el valor de la propiedad y se recoge en el updated()
        this.name = e.target.value
    }
    updateYearsInCompany (e) {
        console.log("updateYearsInCompany")
        // Cambiamos el valor de la propiedad y se recoge en el updated()
        this.yearsInCompany = e.target.value
    }

    updatePersonInfo () {
        console.log("updatePersonInfo")
        console.log("yearsInCompany vale " + this.yearsInCompany)

        if (this.yearsInCompany >= 7) {
            this.personInfo = "lead"
        } else if (this.yearsInCompany >= 5) {
            this.personInfo = "senior"
        } else if (this.yearsInCompany >= 3) {
            this.personInfo = "team"
        } else {
            this.personInfo = "junior"
        }
    }
}

customElements.define('ficha-persona', FichaPersona)