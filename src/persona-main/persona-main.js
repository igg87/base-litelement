import { LitElement, html } from 'lit-element'
import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js'

class PersonaMain extends LitElement {

    static get properties () {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        }
    }

    constructor () {
        super()

        this.showPersonForm = false
        this.people = [
            {
                name: "Ellen Ripley",
                yearsInCompany: 10,
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                photo: {
                    src: "./img/persona1.png",
                    alt: "Ellen Ripley"
                }
            },
            {
                name: "Bruce Banner",
                yearsInCompany: 2,
                profile: "Lorem ipsum Test",
                photo: {
                    src: "./img/persona2.png",
                    alt: "Bruce Banner"
                }
            },
            {
                name: "Éowyn",
                yearsInCompany: 5,
                profile: "Lorem ipsum 55 dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                photo: {
                    src: "./img/persona3.jpg",
                    alt: "Éowyn"
                }
            },
            {
                name: "Turanga Leela",
                yearsInCompany: 9,
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor",
                photo: {
                    src: "./img/persona1.png",
                    alt: "Turanga Leela"
                }
            },
            {
                name: "Tyrion Lannister",
                yearsInCompany: 1,
                profile: "Lorem ipsum 1 dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                photo: {
                    src: "./img/persona2.png",
                    alt: "Tyrion Lannister"
                }
            }
        ]
    }

    render () {
        // NOTA: Los parametros que pasamos por etiqueta deben ser string siempre. Si es un object, se pone un punto delante
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person => html`
                            <persona-ficha-listado
                                fname="${person.name}"
                                yearsInCompany="${person.yearsInCompany}"
                                profile="${person.profile}"
                                .photo="${person.photo}"
                                @delete-person="${this.deletePerson}"
                                @info-person="${this.infoPerson}"
                            ></persona-ficha-listado>
                        `
                    )}
                </div>
            </div>
            <div class="row">
                <persona-form
                    @persona-form-close="${this.personFormClose}"
                    @persona-form-store="${this.personFormStore}"
                    class="d-none border rounded border-primary" id="personForm"></persona-form>
            </div>
        `
    }

    updated (changedProperties) {
        console.log("updated")
        // Buscamos cambios en una variable concreta
        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el showPersonForm en persona-main")
            if (this.showPersonForm === true) {
                this.showPersonFormData()
            } else {
                this.showPersonList()
            }            
        }

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-main")

            this.dispatchEvent(new CustomEvent("updated-people", {
                    "detail": {
                        people: this.people
                    }
                }
            ))
        }
    }

    deletePerson (e) {
        console.log("deletePerson - persona-main")
        console.log("Se va a borrar la persona de nombre " + e.detail.name)
        
        // La funcion filter genera un Array(eliminando el recibido) y se vuelve a asignar a this.people
        this.people = this.people.filter(
            person => person.name != e.detail.name
        )
    }

    infoPerson (e) {
        console.log("infoPerson - persona-main")
        console.log("Se ha pedido mas informacion de la persona de nombre " + e.detail.name)

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        )
        console.log(chosenPerson)

        let personToShow = {}
        personToShow.name = chosenPerson[0].name
        personToShow.profile = chosenPerson[0].profile
        personToShow.yearsInCompany = chosenPerson[0].yearsInCompany

        this.shadowRoot.getElementById("personForm").person = personToShow
        this.shadowRoot.getElementById("personForm").editingPerson = true
        this.showPersonForm = true
    }

    showPersonFormData () {
        console.log("showPersonFormData")
        console.log("Mostrando el formulario de persona")
        this.shadowRoot.getElementById("personForm").classList.remove("d-none")
        this.shadowRoot.getElementById("peopleList").classList.add("d-none")
    }

    showPersonList () {
        console.log("showPersonFormData")
        console.log("Mostrando el listado de personas")
        this.shadowRoot.getElementById("personForm").classList.add("d-none")
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none")
    }

    personFormClose () {
        console.log("personFormClose")
        console.log("Se ha cerrado el formulario de la persona")
        this.showPersonForm = false
    }

    personFormStore (e) {
        console.log("personFormStore")
        console.log("Se va a almacenar una persona")

        console.log("La propiedad name en person vale " + e.detail.person.name)
        console.log("La propiedad Profile en person vale " + e.detail.person.profile)
        console.log("La propiedad yearsInCompany en person vale " + e.detail.person.yearsInCompany)
        console.log("La propiedad editingPerson vale " + e.detail.editingPerson)

        if (e.detail.editingPerson === true) {
            console.log("Se va a actualizar la persona de nombre " + e.detail.person.name)

            this.people = this.people.map(
                person => person.name === e.detail.person.name ? person = e.detail.person : person
            )
            /*
            // Se cambia el codigo por lo de arriba para cambiar el array y que lo coja el updated
            let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.person.name
            )
            if (indexOfPerson >= 0) {
                console.log("persona encontrada")
                this.people[indexOfPerson] = e.detail.person
            }
            */
        } else {
            console.log("Se va a almacenar una persona nueva")
            // Se refactoriza para que this.people cambie y salga por el updated
            // this.people.push(e.detail.person)
            this.people = [...this.people, e.detail.person]
        }
        console.log("Persona almacenada")
        this.showPersonForm = false
    }
}

customElements.define('persona-main', PersonaMain)