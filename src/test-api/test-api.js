import { LitElement, html } from 'lit-element'

class TestApi extends LitElement {

    static get properties () {
        return {
            movies: {type: Array}
        }
    }

    constructor () {
        super ()

        this.movies = []
        this.getMovieData()
    }

    render () {
        // El render pinta lo que se visualiza por pantalla
        // La comilla ` permite presentar el html en modo multi-linea
        return html`
            ${this.movies.map(
                movie => html`<div>La película ${movie.title}, fue dirigida por ${movie.director}</div>`
            )}
        `
    }

    getMovieData () {
        console.log("getMovieData")
        console.log("Obteniendo datos de las peliculas")
        // https://swapi.dev/api/films/
        let xhr = new XMLHttpRequest()

        // El open no realiza la peticion AJAX, solo la inicializa a la request
        xhr.open("GET", "https://swapi.dev/api/films/")

        // Prepara la funcion que se ejecutará cuando se reciba la respuesta de la llamada
        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Petición completada correctamente")

                let APIResponse = JSON.parse(xhr.responseText)
                // console.log(APIResponse)
                // el dato esta en .results dentro del response de esta peticion (esto cambia en cada peticion)
                this.movies = APIResponse.results
            }
        }

        // Realiza la peticion AJAX
        // Si fuese un POST, el body se incluye en el xhr.send({...})
        xhr.send()

        console.log("Fin de getMovieData")
    }
}

customElements.define('test-api', TestApi)